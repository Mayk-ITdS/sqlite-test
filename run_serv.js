const Hapi = require('@hapi/hapi');
const sqlite3 = require("sqlite3");
const db = new sqlite3.Database('./sakila_master.db');


const init = async () => {
    
    
    const server = Hapi.server({
        port: 3000, 
        host: 'localhost' 
    });

    
    server.route({
        method: 'GET',
        path: '/movies',
        handler: async (request, h) => { 

            const query=await new Promise((resolve,reject) => {

                db.all('select TITLE from FILM', (error,rows) => {
                    if (error){

                    reject (error);
                }
                    else{
                    resolve (rows);
                    }
                    
                });   
    }); 
    return query;    
        }});
    // server.route({
    //     method: 'GET',
    //     path: '/',
    //     handler: async (request, h) => { 
    //         const results = db.queryDatabase('Select title FROM FILM where film_id is 1');
    //         return results
    //     }
    // });


    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});


init();