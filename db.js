const sqlite3 = require("sqlite3").verbose();
const filepath = "C:\\Myproject-2\\sqlite-test\\sakila_master.db";

function createDbConnection() {
    const db = new sqlite3.Database(filepath, sqlite3.OPEN_READWRITE, (error) => {
        if (error) {
            console.error("Connection failed: " + error.message);
            throw error;
        } else {
            console.log("Connection with SQLite has been established.");
        }
    });
    return db;
}


const db = createDbConnection();


db.serialize(() => {
    db.all("SELECT * FROM FILM LIMIT 10", [], (error, rows) => {
        if (error) {
            console.error("Query failed: " + error.message);
            throw error;
        }

        
        console.log("Query Results:");
        rows.forEach((row) => {
            console.log(row);
        });

        
        db.close((error) => {
            if (error) {
                console.error("Error closing the database: " + error.message);
            } else {
                console.log("Database connection closed.");
            }
        });
    });
});